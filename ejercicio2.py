# Persona: Realizar un programa que tenga una clase Persona con las siguientes características. 
# La clase tendrá como atributos el nombre y la edad de una persona. Implementar los métodos necesarios para inicializar los atributos,
# mostrar los datos e indicar si la persona es mayor de edad o no.

class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def set_edad(self, edad):
        self.edad = edad
        
    
    def get_edad(self):
        return self.edad
    
persona1 = Persona("simon", 11)
if persona1.edad >= 18:
    print("es mayor de edad")
else:
    print("es menor de edad")