# Realizar un programa en el cual se declaren dos valores 
# enteros por teclado utilizando el método __init__. Calcular después la suma,
# resta, multiplicación y división. Utilizar un 
# método para cada una e imprimir los resultados obtenidos. Llamar a la clase Calculadora.

class Calculadora:
    def __init__(self, numero1, numero2):
        self.numero1 = numero1
        self.numero2 = numero2

    def Suma(self):
        suma = self.numero1 + self.numero2
        return suma

    def Resta(self):
        resta = self.numero1 - self.numero2

    def Multiplicacion(self):
        multiplicacion = self.numero1 * self.numero2

    def Division (self):
        division = self.numero1 / self.numero2

numero1 = int(input("escriba un numero: "))
numero2 = int(input("escriba otro numero: "))
operacion = input("escriba la operacion que quiere realizar: ")
Metodo = Calculadora(numero1, numero2)

if operacion == "suma":
    
    print("la suma es: " + Metodo.Suma())
elif operacion == "resta":
    
    print("la resta es: " + Metodo.Resta())
elif operacion == "division":
    
    print("la division es: " + Metodo.Division())

elif operacion == "multiplicacion":
    
    print("la multiplicacion es: " + Metodo.Multiplicacion())