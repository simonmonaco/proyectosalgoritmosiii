class Mascota:
    def __init__(self, nombre, raza, edad):
        self.nombre = nombre
        self.raza = raza
        self.edad = edad

    def Saludar(self):
        
        return f"la raza es {self.raza}, el nombre es {self.nombre} y la edad es {self.edad}"
    
Mascota1 = Mascota("firulais", "dalmata", 13)

print(Mascota1.Saludar())