﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo1
{
    class Lista
    {
        public Nodo inicio;

        public Lista()
        {
            inicio = null;

        }
        public void InsertarFinal(int item)
        {
            Nodo aux = new Nodo();
            aux.dato = item;
            aux.siguiente = null;

            if(inicio == null)
            {
                inicio = aux;
            }
            else
            {
                Nodo puntero;
                puntero = inicio;
                while(puntero.siguiente == null)
                {
                    puntero = puntero.siguiente;
                }
                puntero.siguiente = aux;
            }
        }
        public void InsertarInicio(int item)
        {
            Nodo aux = new Nodo();

            aux.dato = item;
            aux.siguiente = null;

            if(inicio == null)
            {
                inicio = aux;
            }
            else
            {
                Nodo puntero;
                puntero = inicio;
                inicio = aux;
                aux.siguiente = puntero;
            }
        
        }
        public void EliminarCabeza(int item)
        {
            if(inicio == null)
            {
                Console.WriteLine("lista vacia, no se puede eliminar elemento");
            }
            else
            {
                inicio = inicio.siguiente;
            }
        }
        public void EliminarCola(int item)
        {
            if(inicio == null)
            {
                Console.WriteLine("el nodo esta vacio, no se puede eliminar elemento");
            }
            else
            {

            }
        }
    }
}
